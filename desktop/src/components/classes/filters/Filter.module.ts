// Filter module implements IFilter interface and concrete class for all filter classes
// imageBuffer array contains input image data
// Apply method can call as many internal or external programs
    // This method must return the output data or a boolean if failed
// ApplyMask allows all filters to be masked
    // Masks are standard so they will be implimented here in the concrete class
// Projection requires thumbnail, min and max values for filter panel preview

export interface IProjection {
}

export interface IFilter {
    imageBuffer: Int32Array
    readonly ui: Array< Array<Object> >,
    Apply(imageBuffer: Int32Array, options: Object): Promise<Int32Array | Boolean>
    ApplyMask(imageBuffer: Int32Array, options: Object): Promise<Int32Array | Boolean>
    Projection(): IProjection
}

export class Filter {
    public imageBuffer = new Int32Array()

    async ApplyMask(imageBuffer: Int32Array, options: Object) {
        return new Promise(() => { return true })
            .then(() => { return new Int32Array() })
            .catch(() => { return false })
    }
}