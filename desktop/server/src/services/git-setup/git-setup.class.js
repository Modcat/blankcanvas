const { execSync } = require('child_process')
const fs = require('fs');

exports.GitSetup = class GitSetup {

  constructor () {
    
    this.dir = `${execSync( `echo ${ process.platform === 'win32'? '%USERPROFILE%' : '$HOME'}`, { shell: true } ).toString().trim()}/.blankcanvas`
      
    this.dirPreferences = `${this.dir}/blankcanvas-preferences`
  }

  async get (id) {

    const check = `${execSync( `ssh -i ${this.dir}/access -T git@gitlab.com`, { shell: true } )}`
    
    return check.match('Welcome') ? true : false
  }

  async create () {
    
    if ( !fs.existsSync(this.dir) )
      fs.mkdirSync(this.dir)
    
    if ( !fs.existsSync(`${this.dir}/access`) )
      execSync( `ssh-keygen -f ${this.dir}/access -t ed25519 -N ''`, { shell: true } )

    if ( !fs.existsSync(this.dirPreferences) ) {
      
      fs.mkdirSync(this.dirPreferences)

      fs.openSync(`${this.dirPreferences}/Readme.md`, 'w')

      execSync( 'git init && git add . && git commit -m "Blankcanvas Ready!"', { shell: true, cwd: this.dirPreferences } )
    }

    return `${fs.readFileSync( `${this.dir}/access.pub` )}`
  }

  async update (id, data, params) {

    execSync(`git config user.name ${data.username}`, { shell: true, cwd: this.dirPreferences } )

    execSync(`git config user.email ${data.email}`, { shell: true, cwd: this.dirPreferences } )
    
    const command = `GIT_SSH_COMMAND='ssh -i ${this.dir}/access' git push --set-upstream git@gitlab.com:${data.username}/blankcanvas-preferences.git master`
    
    return `${execSync( command, { shell: true, cwd: this.dirPreferences } )}`;
  }
};
