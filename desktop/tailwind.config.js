module.exports = {
  purge: {
    content: ['./public/**/*.html', './src/**/*.vue'],
    options: {
      whitelistPatterns: [
        /-(leave|enter|appear)(|-(to|from|active))$/,
        /^(?!(|.*?:)cursor-move).+-move$/,
        /^router-link(|-exact)-active$/
      ],
    },
  },
  darkMode: false, // or 'media' or 'class'
  theme: {
    colors: {
      gray: {
        50: '#F9FAFB',
        100: '#F3F4F6',
        200: '#E5E7EB',
        300: '#D1D5DB',
        400: '#9CA3AF',
        500: '#6B7280',
        600: '#4B5563',
        700: '#374151',
        800: '#1F2937',
        900: '#111827',
      },
      tags: {
        violet: '',
        indigo: '',
        blue: '#0024FC',
        green: '#31cf64',
        yellow: '',
        orange: '#ffa666',
        red: '#ef304d',
      },
      white: 'white',
      cta: '#211CE7',
      color: '#c1c1c1',
      colorInverted: 'white',
      panel: '#232323',
      panelBorder: '#000000',
      panelLine: '',
      backgroundAccent: '#313131',
      overlay: '',
      overlayAccent: '',
      iconFill: '',
      iconFillActive: '#2292f1',
      iconStroke: '',
      iconStrokeActive: '#2292f1',
    },
    extend: {},
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
