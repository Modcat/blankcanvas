/*
In NativeScript, a file with the same name as an XML file is known as
a code-behind file. The code-behind is a great place to place your view
logic, and to set up your page’s data binding.
*/

import {
    EventData,
    Page,
    ItemEventData,
    ObservableArray,
    WebView,
} from "@nativescript/core";
import { MainPageModel } from "./main-view-model";
import { MainMenu } from "./Models/MainMenu";
import { Global } from "./Global";

// Event handler for Page 'navigatingTo' event attached in main-page.xml

export function navigatingTo(args: EventData) {
    const page = <Page>args.object;
    var webView: WebView = page.getViewById("editorView");
    setTimeout(() => {
        if (page.android) {
            webView.android.getSettings().setBuiltInZoomControls(false);
        }
    }, 200);

    Global.MainPageBinding = new MainPageModel();
    page.bindingContext = Global.MainPageBinding;
}

export function onItemTap(args: ItemEventData) {
    //const index = args.index;
    //const tappedItemView = args.view;
    //const mainMenus: ObservableArray<MainMenu> = <ObservableArray<MainMenu>>mainPageBinding.mainMenus;
    //mainMenus.toggleOpen();
}
