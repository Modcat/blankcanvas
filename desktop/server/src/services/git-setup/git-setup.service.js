// Initializes the `git-setup` service on path `/git-setup`
const { GitSetup } = require('./git-setup.class');
const hooks = require('./git-setup.hooks');

module.exports = function (app) {
  const options = {
    paginate: app.get('paginate')
  };

  // Initialize our service with any options it requires
  app.use('/git-setup', new GitSetup(options, app));

  // Get our initialized service so that we can register hooks
  const service = app.service('git-setup');

  service.hooks(hooks);
};
