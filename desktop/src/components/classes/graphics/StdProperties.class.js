// Standard properties and getters and setters for all objects on the canvas

class StdProperties {
    #id
    #x
    #y
    #z
    #w
    #h
    #rx
    #ry
    #rz
    #tx
    #ty
    #tz
    constructor(props) {
        for (const prop in props)
        {
            if (typeof this[prop] === 'function')
                this[prop](props[prop])
        }
    }
    id(str) {
        if (str.contructor.name !== 'String')
            throw 'This must be a string'
        this.#id = str
    }
    x(int) {
        if (int.contructor.name !== 'Number')
            throw 'This must be a number'
        this.#x = int
    }
    y(int) {
        if (int.contructor.name !== 'Number')
            throw 'This must be a number'
        this.#y = int
    }
    z(int) {
        if (int.contructor.name !== 'Number')
            throw 'This must be a number'
        this.#z = int
    }
    w(int) {
        if (int.contructor.name !== 'Number')
            throw 'This must be a number'
        this.#w = int
    }
    h(int) {
        if (int.contructor.name !== 'Number')
            throw 'This must be a number'
        this.#h = int
    }
    rx(int) {
        if (int.contructor.name !== 'Number')
            throw 'This must be a number'
        this.#rx = int
    }
    ry(int) {
        if (int.contructor.name !== 'Number')
            throw 'This must be a number'
        this.#ry = int
    }
    rz(int) {
        if (int.contructor.name !== 'Number')
            throw 'This must be a number'
        this.#rz = int
    }
    tx(int) {
        if (int.contructor.name !== 'Number')
            throw 'This must be a number'
        this.#tx = int
    }
    ty(int) {
        if (int.contructor.name !== 'Number')
            throw 'This must be a number'
        this.#ty = int
    }
    tz(int) {
        if (int.contructor.name !== 'Number')
            throw 'This must be a number'
        this.#tz = int
    }
}