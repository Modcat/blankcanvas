// foo.ts
interface Square {
    size: number;
    color?: string;
}