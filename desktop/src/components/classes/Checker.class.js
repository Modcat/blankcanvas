// Checker contains static methods for checking values and interfaces, this will be used globally throughout the classes and interface. Allmembers must be static.

class Checker {

    static #types = ['String', 'Number', 'Array', 'Object', 'Boolean', 'Function']
    // Field contain's all global interfaces, keys will be interface name
    static #interfaces = {}

    //// Check primitive values

    static CheckString(value = false, options = { min: false, max: false }) {
        if (value.constructor.name !== 'String')
            throw `Sorry "${value}" is not a string`

        if (options.min.constructor.name === 'Number' && value.length < options.min)
            throw `Sorry "${value}" is not long enough`

        if (options.max.constructor.name === 'Number' && value.length > options.max)
            throw `Sorry "${value}" is too long`

        return true
    }
    
    static CheckNumber(value = false, options = { min: false, max: false, float: true, minDecimal: false, maxDecimal: false }) {
        if (value.constructor.name !== 'Number')
            throw `Sorry "${value}" is not a number`

        if (options.min.constructor.name === 'Number' && value < options.min)
            throw `Sorry number "${value}" is to low`

        if (options.max.constructor.name === 'Number' && value > options.max)
            throw `Sorry number "${value}" is to high`

        if (options.float === false && value % 1 != 0)
            throw `Sorry number "${value}" must be an int and not a float`

        const decimalNumbers = ParseInt(value.toString().split('.')[1])

        if (options.minDecimal.constructor.name === 'Number' && decimalNumbers < options.minDecimal)
            throw `Sorry number "${value}" must have ${options.minDecimal} decimal places`

        if (options.maxDecimal.constructor.name === 'Number' && decimalNumbers > options.maxDecimal)
            throw `Sorry number "${value}" must have ${options.maxDecimal} decimal places`

        return true
    }
    
    static CheckBoolean(value = false) {
        if (value.constructor.name !== "Boolean")
            throw `Sorry "${value}" is not a boolean`

        return true
    }

    static CheckFunction(value = false, returnType = false) {
        if (value.constructor.name !== 'Function')
            throw `"${value}" is not a function`

        if (this.#types.indexOf(returnType) === -1)
            throw `The type "${returnType}" is not valid`

        return true
    }

    //// Basic object, array or enum checks

    // CheckEnum is for strings or numbers that must be a limited range of outputs
    static CheckEnum(Enum = false, value = false, type = 'String') {
        if (Enum.constructor.name === 'Array')
            throw `Enum is invalid`

        if (this.#types.indexOf(type) === -1)
            throw `The type "${type}" is not valid`

        if (this.CheckArray(Enum, 'String'))
            throw `Enum ${Enum} is invalid, must be an array of strings`

        if (value === false)
            throw `Value invalid`

        if (value.constructor.name === type)
            throw `Value "${value}" is not of type "${type}"`

        if (Enum.indexOf(value) === -1)
            throw `Value "${value}" is not in the enum: "${Enum}"`

        return true
    }

    // Object & array checking
    static CheckArray(arr = false, type = false) {
        if (arr.constructor.name === 'Array')
            throw `Array is invalid`

        if (this.#types.indexOf(type) === -1)
            throw `The type "${type}" is not valid`

        if (interface)
        {

        }

        return true
    }

    static CheckObject(value = false, interface = false) {
        if (value.constructor.name !== 'Object')
            throw `Value is invalid`
        
        if (interface.constructor.name !== 'Object')
            throw `Interface is invalid`

        if (interface)
        {
            const interfaceKeys = Object.keys(interface)
        
            for (let property in valueKeys)
            {
                // Check keys exist in interface
                if (interfaceKeys.indexOf(property) === -1)
                    throw `Property ${property} to interface ${interface}`

                // Check primitive properties value
                if ()
                    throw ``

                // Check elements in array, primitive types only

                // Check readonly properties
            }
            
        }
        return true
    }

    //// Interfaces

    static #ConvertKey(name, property) {

        let propertyInterface = {}

        if (property.match(/\?$/gi))
            propertyInterface.optional = true

        if (property.match(/^readonly\s+/gi))
            propertyInterface.readOnly = true

        propertyInterface.key = property
            .replace('readonly ', '')
            .replace('?', '')
    }

    static #ConvertValue(name, values) {
        let propertyValue = { values: [] }

        // Treat everything as an array
        if (values.constructor.name === 'Array')
            propertyValue.values = [...values]
        else
            propertyValue.values.push(values)

        // Loop each value and convert to { type: '...', interface: '...' || null }
        for (let index = 0; index <  propertyValue.values; index++) {

            const value = propertyValue[index]

            // Check type is valid
            if (this.#types.indexOf(value.split('<')[0]))
                throw `Wrong type give in interface ${name}`
            
            propertyValue[index] = {
                type: value.split('<')[0],
                interface: value.match('<') ? value.split('<')[1].replace('>', '') : null
            }
        }

        return propertyValue
    }
    
    // Convert interface to JS
    static #ConvertInterface(name, interface = false) {
        // Convert to test
        var example1 = {
            'readonly name': 'String',
            'readonly age': 'Number',
            'readonly isOld?': 'Boolean',
            'addFriend': 'Function',
            'license': 'Object',
            'fingers': 'Array',
            'friends': 'Array<Person>',
        }

        if (name == null)
            throw `Interface name required`

        // Loop over all key and value pairs and convert to JS readable code
        let convertedInterface = []

        for (const property in interface) {
            
            // Convert key and value pairs to JS and merge them
            this.#interfaces[name] = Object.assign(
                this.#ConvertKey(name, property),
                this.#ConvertValue(name, interface[property])
            )
        }
    }
    
    static addInterface(name, interface) {
        this.#ConvertInterface(name, interface)
    }
    
    // Fetch & convert interfaces from FeathersJS (Blankcanvas specific)
    static #CheckFeathers() {
        
    }

    // Run interface check
    static CheckInterface(value = false, interface = false) {
        
    }
}