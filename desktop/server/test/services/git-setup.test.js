const assert = require('assert');
const app = require('../../src/app');

describe('\'git-setup\' service', () => {
  it('registered the service', () => {
    const service = app.service('git-setup');

    assert.ok(service, 'Registered the service');
  });
});
