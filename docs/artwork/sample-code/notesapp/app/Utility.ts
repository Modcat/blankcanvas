import { View, ListView } from "@nativescript/core";
interface PopupItem{
    icon?: string;
    label: string;
}
let prevMenu:any;
export function PopupMenu(obj:View, popupItems: Array<PopupItem>){
    const popupContainer: ListView = obj.page.getViewById("popupContainer");
    if(prevMenu == obj){
        prevMenu = null;
        popupContainer.visibility = 'collapse';
        return false;
    }
    prevMenu = obj;
    popupContainer.page.on('tap', function(){
         popupContainer.visibility = 'collapse';
    })
    popupContainer.visibility = 'collapse';
    popupContainer.translateX = obj.getLocationOnScreen().x-<number>popupContainer.width+<number>obj.width-10;
    popupContainer.translateY = 25;

    popupContainer.set("items", popupItems);
    popupContainer.visibility = 'visible';
}