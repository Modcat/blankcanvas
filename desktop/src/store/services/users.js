export const users = {
    state: () => ({
        username: 'Lawrence'
    }),
    mutations: {
        io(state, payload) {
            state.users[payload.id] = payload
        },
        removed(state, payload) {
            delete state.users[payload.id]
        }
    },
    actions: {
        io({ commit }, payload) {
            commit('io', payload)
        },
        removed({ commit }, payload) {
            commit('removed', payload)
        }
    }
}
