import { createStore } from 'vuex'
import { users } from './services/users'
import { feathers } from './feathers'

export const store = createStore({
  getters: {
    vuexClient: () => window.client
  },
  modules: {
    users,
    feathers
  }
})