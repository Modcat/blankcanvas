import { Observable, ObservableArray } from "@nativescript/core";
import { MainMenu } from "./MainMenu";
import { Global } from "~/Global";

export class SubMenu extends Observable {
    constructor() {
        super();
    }

    public get Title(): string {
        return this.get("_title");
    }
    public set Title(v: string) {
        this.set("_title", v);
    }

    public get Active(): boolean {
        return this.get("_Active");
    }
    public set Active(v: boolean) {
        this.set("_Active", v);
    }

    public toggleActive() {
        for (let i = 0; i < Global.MainPageBinding.mainMenus.length; i++) {
            const menu = Global.MainPageBinding.mainMenus.getItem(i);
            if (menu != null) {
                if(menu.subMenus){
                    for (let j = 0; j < menu.subMenus.length; j++) {
                        const submenu = menu.subMenus.getItem(j);
                        submenu.set("Active", false);
                    }
                }                
            }
        }
        this.set("Active", true);
    }
}
