module.exports = {
  lintOnSave: true,
  outputDir: 'server/public',
  assetsDir: 'src/assets',
  devServer: {
      port: 1993
  },
  css: {
    sourceMap: true
  },
}