import { Observable, ObservableArray, EventData, Button } from "@nativescript/core";
import { MainMenu } from "./Models/MainMenu";
import { SubMenu } from "./Models/SubMenu";
import { NoteTopic } from "./Models/NoteTopic";
import { PopupMenu } from "./Utility";

export class MainPageModel extends Observable {
    constructor() {
        super();
        this.mainMenus = new ObservableArray<MainMenu>();
        this.NoteTopics = new ObservableArray<NoteTopic>();
        this.LoadMenus();
        this.LoadTopics();
    }

    private LoadMenus() {
        var mainMenu1 = new MainMenu();
        mainMenu1.Title = "Shortcuts";
        mainMenu1.Icon = "e9e9";
        mainMenu1.IconClass = "fo";
        this.mainMenus.push(mainMenu1);

        var mainMenu2 = new MainMenu();
        mainMenu2.Title = "Lists";
        mainMenu2.Icon = "ea16";
        mainMenu2.IconClass = "fo";
        this.mainMenus.push(mainMenu2);

        var mainMenu3 = new MainMenu();
        mainMenu3.Title = "Notebooks";
        mainMenu3.Icon = "e828";
        mainMenu3.IconClass = "li";
        mainMenu3.subMenus = new ObservableArray<SubMenu>();

        var submenu1 = new SubMenu();
        submenu1.Title = "Test drive";
        submenu1.Active = true;
        mainMenu3.subMenus.push(submenu1);

        var submenu2 = new SubMenu();
        submenu2.Title = "Test drive 2";
        mainMenu3.subMenus.push(submenu2);

        this.mainMenus.push(mainMenu3);

        var mainMenu4 = new MainMenu();
        mainMenu4.Title = "Shared Notebooks & Lists";
        mainMenu4.Icon = "e82b";
        mainMenu4.IconClass = "li";
        mainMenu4.subMenus = new ObservableArray<SubMenu>();

        var submenu4 = new SubMenu();
        submenu4.Title = "Shared 1";
        submenu4.Active = false;
        mainMenu4.subMenus.push(submenu4);
        this.mainMenus.push(mainMenu4);

        var mainMenu5 = new MainMenu();
        mainMenu5.Title = "Tags";
        mainMenu5.Icon = "e82f";
        mainMenu5.IconClass = "li";
        this.mainMenus.push(mainMenu5);

        var mainMenu6 = new MainMenu();
        mainMenu6.Title = "Trash";
        mainMenu6.Icon = "e9c9";
        mainMenu6.IconClass = "fo";
        this.mainMenus.push(mainMenu6);
    }

    private LoadTopics() {
        const topics = [
            {
                timestamp: new Date(),
                title: "Useful Plugins / Scripts",
                sub: "Vue field validation",
                active: true
            },
            {
                timestamp: new Date(),
                title: "SSH",
                sub: "Documentation",
            },
            {
                timestamp: new Date(),
                title: "Feathers",
                sub: "General notes App General",
            },
            {
                timestamp: new Date(2020, 9, 8, 12, 50),
                title: "Fixing Errors",
                sub: "Node-SASS detecting errors testing length",
            }
        ];
        for (let i = 0; i < topics.length; i++) {
            const topic = topics[i];
            const topic1 = new NoteTopic();
            topic1.TopicTitle = topic.title;
            topic1.TopicSubtitle = topic.sub;
            topic1.Timestamp = topic.timestamp;
            topic1.Active = topic.active;

            this.NoteTopics.push(topic1);
        }
    }

    public showMenu1(args: EventData){ 
        PopupMenu((<Button>args.object),[
            {label: "Menu Item 1"},
            {label: "Menu Item 2"},
            {label: "Menu Item 3"},
        ]);      
    }

    public showMenu2(args: EventData){ 
        PopupMenu((<Button>args.object),[
            {label: "Menu Item 3"},
            {label: "Menu Item 4"},
            {label: "Menu Item 5"},
        ]);      
    }

    public get mainMenus(): ObservableArray<MainMenu> {
        return this.get("_mainMenus");
    }
    public set mainMenus(v: ObservableArray<MainMenu>) {
        this.set("_mainMenus", v);
    }

    public get NoteTopics(): ObservableArray<NoteTopic> {
        return this.get("_NoteTopics");
    }
    public set NoteTopics(v: ObservableArray<NoteTopic>) {
        this.set("_NoteTopics", v);
    }
}
