// Tween is for instance level animation applying to timeline clips audio and all graphical elements.

class Tween {
    // Type of animation could be graphical or audio
    #type
    // Each element in the array represents a key frame
    #keyframes = []
    
    constructor(props) {
        for (const prop in props)
        {
            if (typeof this[prop] === 'function')
                this[prop](props[prop])
        }
    }

    setType(str) {
        // Type of animation could be graphical or audio
        if ( str === 'graphical' || str === 'audio' )
            throw 'This must be a string of either graphical or audio'
        this.#type = str
    }
    
    // When setting the keyframes order them by timestamp
    addKeyframes(keyframes) {}

    updateKeyframes(keyframes) {}

    removeKeyframes(keyframes) {}

    // Getters
    getTimeline() {
        return this.#keyframes
    }
}