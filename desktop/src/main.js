import { createApp } from 'vue'
import { store } from './store/index'
import App from './App.vue'
import './assets/styles/base.less'

// Global components

import Image from './components/global/Image.vue'
import MainButton from './components/global/MainButton.vue'
import InputStd from './components/global/InputStd.vue'

// Plugins

require('./plugins/nativescript-elements')

// App

const app = createApp(App)

app.config = {
    productionTip: false
}

app.use(store)

app.component('Image', Image)
app.component('MainButton', MainButton)
app.component('InputStd', InputStd)

app.mount('#app')
