import { Observable, ObservableArray } from "@nativescript/core";
import { SubMenu } from "./SubMenu";

export class MainMenu extends Observable {

    constructor() {
        super();

    }

     public get subMenus() : ObservableArray<SubMenu> {
        return this.get("_subMenus");
    }
    public set subMenus(v: ObservableArray<SubMenu>)  {
        this.set("_subMenus", v);
    }

    public get Title() : string {
        return this.get("_title");
    }
    public set Title(v:string) {
        this.set("_title", v) ;
    }
    
    public get IconClass() : string {
        return this.get("_iconClass");
    }
    public set IconClass(v:string) {
        this.set("_iconClass", v) ;
    }
    
    public get Opened() : boolean {
        return this.get("_Opened");
    }
    public set Opened(v:boolean) {
        this.set("_Opened", v) ;
    }

    public get Icon(): string {
        return String.fromCharCode(parseInt(this.get("_icon"), 16));
    }
    public set Icon(v: string) {
        this.set("_icon", v);
    }

    public toggleOpen(){
        this.set("Opened", !this.get("Opened"));       
    }
    
}