// Nativescript layout
document.createElement('AbsoluteLayout')
document.createElement('DockLayout')
document.createElement('FlexboxLayout')
document.createElement('GridLayout')
document.createElement('StackLayout')
document.createElement('WrapLayout')

// Nativescript action bar (disbaled on desktop)
// ActionBar
// ActionItem
// NavigationButton

// Nativescript components
document.createElement('ActivityIndicator')
document.createElement('Button')
document.createElement('DatePicker')
document.createElement('Image')
document.createElement('Label')
document.createElement('ListPicker')
document.createElement('ListView')
document.createElement('Progress')
document.createElement('ScrollView')
document.createElement('SearchBar')
document.createElement('SegmentedBar')
document.createElement('Slider')
document.createElement('Switch')
document.createElement('TabView')
document.createElement('TextField')
document.createElement('TextView')

// Nativscript disabled components strictly NOT ALLOWED
// HtmlView
// WebView
// TimePicker
// Page
// Placeholder