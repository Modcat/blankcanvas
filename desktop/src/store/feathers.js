export const feathers = {
    namespaced: true,
    state: () => ({
        client: false,
        authenticate: false,
        authenticated: false,
        services: false,
    }),
    mutations: {
        CLIENT: (state, payload) => state.client = payload,

        AUTHENTICATE: (state, payload) => state.authenticate = payload,

        AUTHENTICATION: (state, payload) => state.authentication = payload,
        
        SERVICES: (state, payload) => state.services = payload,
    },
    actions: {
        async connectToFeathers({ commit }) {
            
            fetch(`http://${document.location.hostname}:1992`)
            .then(() => {

                const io = require('socket.io-client')
                const feathers = require('@feathersjs/client')
                const socket = io(`http://${document.location.hostname}:1992`, { forceNew: true })
  
                const client = feathers()
                client.configure(feathers.socketio(socket))

                client.configure(feathers.authentication({
                    storage: window.localStorage
                }))

                window.client = client

                commit('CLIENT', true)
                
                client.on('authenticated', () => {  commit('AUTHENTICATION', client.authentication) })
                
                commit('AUTHENTICATE', client.authenticate)
                
                commit('SERVICES', client.services)
                
                
                // Registered all services in socket and make the appropriate call to Vuex

                // const services = [
                //     'users',
                //     'authentication',
                //     'child-process',
                //     'fonts',
                //     'fs',
                //     'git',
                //     'images',
                //     'messages',
                //     'profile',
                // ]

                
                // // Loop over services
                // services
                // .forEach( serviceName => {
                    
                //     const service = client.service(serviceName)

                //     // Assign event sockets

                //     const events = ['created', 'updated', 'patched', 'removed']

                //     events
                //     .forEach( eventName => {
                //         service
                //         .on(
                //             eventName,
                //             payload => { console.log(payload) }
                //         )
                //     })

                // })
            })
            .catch(() => {
                alert('No network to local Blankcanvas')
            })
        },
    }
}
