# Blankcanvas
### Author: Lawrence Anthony Turton
### Publisher: conceivable media

This is an open project based on the creation of new innovative all in one software type. This software is free to use and distribute under the GNU GPLv3 license. All ideas are not to be patented any attempt to do so will be classed as a violation of intellectual property.

This software is not production quality, it is conceptware and is not intented to be downloaded or used in any capacity. This software has some code that runs the backend and networks over local network.

To compile this application you will need to...

1. Download NodeJS
2. Open a console window
3. Install yarn: npm i -g yarn
4. Download this repo
5. Target the directory where you downloaded this repo
6. Run the follow command: yarn initbc

To run the destop application

1. cd desktop
2. yarn dev

To run the mobile application

1. Install nativescript
2. cd mobile
3. tns preview

In order for you to build or run on an emulator like Android or iOS then please follow the instructions of the nativescript Vue documentation.

## Worthy contributors
1. Project Manager / UI / UX / Framework / Code / Design - Lawrence Turton
2. Lead UI Designer - Lesshan (Lissha55)