import { Observable } from "@nativescript/core/data/observable";
var timeAgo = require("node-time-ago");
export class NoteTopic extends Observable {
    public get Timestamp(): Date | string {
        const ts: Date = this.get("_Timestamp");
        const yr = ts.getFullYear();
        const mon = ts.getMonth();
        const day = ts.getDay();
        const hr = ts.getHours();
        const min = ts.getMinutes();

        const dt = new Date();
        if (
            yr == dt.getFullYear() &&
            mon == dt.getMonth() &&
            day == dt.getDay()
        ) {
            return hr > 12
                ? hr - 12 + ":" + min + " PM"
                : hr + ":" + min + " AM";
        }

        return (
            (day < 10 ? "0" + day : day) +
            "/" +
            (mon < 10 ? "0" + mon : mon) +
            "/" +
            yr
        );
    }
    public set Timestamp(v: Date | string) {
        if (this.get("_Timestamp") != v) this.set("_Timestamp", v);
    }

    public get TopicTitle(): string {
        return this.get("_TopicTitle");
    }
    public set TopicTitle(v: string) {
        if (this.get("_TopicTitle") != v) this.set("_TopicTitle", v);
    }

    public get TopicSubtitle(): string {
        return this.get("_TopicSubtitle");
    }
    public set TopicSubtitle(v: string) {
        if (this.get("_TopicSubtitle") != v) this.set("_TopicSubtitle", v);
    }
    
    public get Active(): boolean {
        return this.get("_Active");
    }
    public set Active(v: boolean) {
        this.set("_Active", v);
    }
}
