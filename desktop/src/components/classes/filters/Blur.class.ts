import type { IFilter } from './Filter.module'
import { Filter } from './Filter.module'

interface IBlur {
}

// Each row is an array
    // Each object in the array is the control on that row
    // Bind uses the internal class propeties (only use getters and strictly type input)
const ui = [
    [
        {title: 'amount', type: 'range', width: '50%', bind: 'amount'},
        {title: 'angle', type: 'radial', width: '50%', bind: 'radius'}
    ]
]

export class Blur extends Filter implements IBlur, IFilter {
    private amount = 0;
    private radius = 0;
    readonly ui = ui;
    
    async Apply(imageBuffer: Int32Array, options: Object) {
        return new Promise(() => { return true })
            .then(() => { return new Int32Array() })
            .catch(() => { return false })
    }

    Projection() {
        return {}
    }
}