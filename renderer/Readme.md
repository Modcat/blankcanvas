# Renderer
The renderer exports graphical assets to web assembly UI. This will display graphics, elements, DOM, audio visualisation, games and more.

We use Heaps.io for basic stuff, also all of openFL is available and we quick export and review using HashLink a virtual machine for Haxe.

Installtion
haxelib install heaps
haxelib install hlc-compiler
(Emscripten will need installation as well)

Compiling to HL
haxe -hl main.hl -lib heaps -main Main

### Run HL
hl main.hl

### Compiling to C code...
haxe --main Main -lib heaps --hl out/main.c

### Compiling to Web Assembly
