const { spawnSync, execSync } = require('child_process')
const electron = require('electron')
const app = electron.app
const path = require('path')
const fetch = require("node-fetch")
const config = {dev: true}

const privateIP = Object
	.values( require('os').networkInterfaces() )
	.flat()
	.filter( inter => { return inter.family === 'IPv4' && !inter.internal })[0].address
process.env.ELECTRON_DISABLE_SECURITY_WARNINGS = true

var URL

spawnSync('pm2 start ./server/src/index.js --name blankcanvas', { shell: true })

URL = `http://${privateIP}:1992`

// Log server address
console.log(URL)

//// Electron

let win = null
const newWin = () => {
	
	win = new electron.BrowserWindow({
		icon: path.join(__dirname, 'static/icon.png'),
		frame: false,
		width:  1280,
		height: 760,
		webPreferences: {
			nodeIntegration: true,
			nodeIntegrationInWorker: false,
			webSecurity: false,
			experimentalFeatures: true
		},
	})

	win.hide()
	
	// Dev mode
	if (config.dev) {
		win.webContents.openDevTools()
	}
	
	win.on('closed', () => win = null)
}

// Wait for PM2 process to occur
const remoteCheck = () => {
	const fetchRemote = setInterval(() => {
		fetch(URL)
		.then(() => clearInterval(fetchRemote))
		.catch(() => null)
	}, 1000)
	setTimeout(() => {
		win.loadURL(URL)
		win.show()
	}, 1000)
}

//// Events
app.on('ready', newWin)
app.on('ready', remoteCheck)

app.on('quit', () => {
	execSync('pm2 delete blankcanvas', { shell: true })
	app.quit()
})

app.on('activate', () => {
	win = newWin()
})

