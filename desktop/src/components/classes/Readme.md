# Classes

## Comments
At the beginning of the file, explain what the class does and generalise what it's members are doing. Also add comments above each member that is not self explanitory to help other developers.

## Constructors
Make sure a constructor only takes an object and uses the standard constructor code. This will ensure error free construction of objects and prevent mismatched types by using members of the class. Try not to use super too often as the data structure should support calling a class when needed rather than needlessly instantiating a class when not needed, ensuring clean and lexical code.

## Validation
To ensure no data annomolies make use of the checker class on values. It provides useful checks which ensures data entegrity.

## Private fields only
Only use private fields and use getters and setters to modigy the value. All setters need to be strict on what values they accept primitive or class based. Make sure constructors and method members enforce strict typing no poloymorhing or casting values, only one value is allowed to be accepted.

## Little inhertiance
keep classes low and close to the wire, use as little inhertiance as possible.

## NOT functional
Classes are not to be used for functional purposes for example drawing a rectangle in WebGL, classes can only be used to store information about the rectangle that later will be stored as JSON.

# Remember our founding fathers

### Single-responsibility
A class should only have a single responsibility, that is, only changes to one part of the software's specification should be able to affect the specification of the class.

### Open–closed
"Software entities ... should be open for extension, but closed for modification."

### Liskov substitution
"Objects in a program should be replaceable with instances of their subtypes without altering the correctness of that program." See also design by contract.

### Interface segregation
"Many client-specific interfaces are better than one
general-purpose
Dependency inversion
One should "depend upon abstractions, not concretions."