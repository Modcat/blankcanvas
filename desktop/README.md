## Build Setup Desktop

Install dependencies yarn and python. Python Linux only command for windows you can download python.exe and install

``` bash 
$ npm i -g yarn
# Linux only
$ sudo apt-get install python
# cd to the relevant directory
$ cd /to/blankcanvas/project/...
$ yarn

# Serve with hot reload at localhost:3000
$ yarn dev

# Build for production and launch server
$ yarn build
$ yarn start

# Generate static project
$ yarn generate
```
